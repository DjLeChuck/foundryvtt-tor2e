export default function () {
    function isMacroExists(macroName, macroContent, icon = undefined) {
        const entities = Array.from(game.macros);
        if (icon) {
            return entities.find(
                m => (m.name === macroName) && (m.data.command === macroContent) && (icon === m.data.img));
        } else {
            return entities.find(m => (m.name === macroName) && (m.data.command === macroContent));
        }
    }

    /**
     * Create a macro when dropping an entity on the hotbar
     * Item      - open roll dialog for item
     * Actor     - open actor sheet
     * Journal   - open journal sheet
     */
    Hooks.on("hotbarDrop", async (bar, data, slot) => {
        // Create item macro if rollable item - weapon, spell, prayer, trait, or skill
        if (data.type === "Item") {
            const item = game.items.get(data.id);
            let itemType = item?.data?.type ?? data?.data?.type;
            let itemName = item?.name ?? data?.data?.name;
            let itemImg = item?.img ?? data?.data?.img;
            if (itemType !== "weapon" && itemType !== "trait" && itemType !== "skill")
                return;
            if (itemType === "weapon") {
                let command = `game.tor2e.macro.utility.rollItemMacro("${itemName}", "${itemType}");`;
                let macro = isMacroExists(itemName, command);
                if (!macro) {
                    macro = await Macro.create({
                        name: itemName,
                        type: "script",
                        img: itemImg,
                        command: command,
                        permission: {default: CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER}
                    }, {displaySheet: false})
                }
                game.user.assignHotbarMacro(macro, slot);
                return;
            }
        }
        // Create a macro to open the actor sheet of the actor dropped on the hotbar
        else if (data.type === "Skill") {
            const skillId = data.data.id;
            const skillKey = data.data.key;
            let command = `game.tor2e.macro.utility.rollSkillMacro("${skillId}");`
            let skillIcon = `systems/tor2e/assets/images/icons/skills/skill-${skillKey}.webp`;
            let macro = isMacroExists(skillId, command, skillIcon);
            if (!macro) {
                macro = await Macro.create({
                    name: skillId,
                    type: "script",
                    img: skillIcon,
                    command: command,
                    permission: {default: CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER}
                }, {displaySheet: false})
            }
            game.user.assignHotbarMacro(macro, slot);
        }
        // Create a macro to open the actor sheet of the actor dropped on the hotbar
        else if (data.type === "Actor") {
            const actor = game.actors.get(data.id);
            let command = `game.actors.get("${data.id}").sheet.render(true)`
            let macro = isMacroExists(actor.name, command);
            if (!macro) {
                macro = await Macro.create({
                    name: actor.data.name,
                    type: "script",
                    img: actor.data.img,
                    command: command,
                    permission: {default: CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER}
                }, {displaySheet: false})
            }
            game.user.assignHotbarMacro(macro, slot);
        }
        // Create a macro to open the journal sheet of the journal dropped on the hotbar
        else if (data.type === "JournalEntry") {
            let journal = game.journal.get(data.id);
            let command = `game.journal.get("${data.id}").sheet.render(true)`
            let macro = isMacroExists(journal.name, command);
            if (!macro) {
                macro = await Macro.create({
                    name: journal.data.name,
                    type: "script",
                    img: "systems/tor2e/assets/images/icons/skill.png",
                    command: command,
                    permission: {default: CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER}
                }, {displaySheet: false})
            }
            game.user.assignHotbarMacro(macro, slot);
        }
        return false;
    });
}
