/**
 * Extend the base TokenDocument class to implement system-specific Community Eye Awareness bar logic.
 * @extends {TokenDocument}
 */
export class Tor2eTokenDocument extends TokenDocument {

    /** @inheritdoc */
    getBarAttribute(...args) {
        const data = super.getBarAttribute(...args);
        if ( data && (data.attribute === "eyeAwareness") ) {
            let overridedEyeAwareness = game.settings.get("tor2e", "overridedEyeAwareness")
            if (overridedEyeAwareness) {
                data.max = overridedEyeAwareness;
            }
        }
        return data;
    }
}
